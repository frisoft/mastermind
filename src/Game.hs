module Game( game ) where

import qualified System.Console.ANSI as T
import System.IO
import Data.List (intercalate, sort)
import System.Random (randomRIO)
-- import Debug.Trace (traceShow)

game :: IO ()
game = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stdin NoBuffering
  T.setSGR [T.SetColor T.Background T.Dull T.Black]
  T.setSGR [T.SetConsoleIntensity T.BoldIntensity]
  putStr viewStandardColor
  randNums <- sequence $ replicate 4 $ randomRIO (1,6::Int)
  inputLoop $ initState randNums
  T.setSGR [T.Reset]

data Colour = Red | Green | Yellow | Blue | Magenta | Cyan deriving (Show, Eq, Ord)

type Code = [Colour]

data Clue = Clue { whites :: Int
                 , blacks :: Int
                 } deriving Show

data ClueColour = White | Black deriving (Show, Eq)

data State = State { code :: Code
                   , currentGuess :: Code
                   , guesses :: [Code]
                   , endGame :: Bool
                   , won :: Bool
                   , lose :: Bool
                   } deriving Show

inputLoop :: State -> IO ()
inputLoop state = do
  putStr $ view state
  c <- getChar
  let state' = update state c
    in if (endGame state')
         then (putStr $ view state')
         else (inputLoop state')

initState :: [Int] -> State
initState randNums = State { code = initCode randNums
                           , currentGuess = []
                           , guesses = []
                           , endGame = False
                           , won = False
                           , lose = False }

initCode :: [Int] -> Code
initCode randNums = map numToColour randNums

numToColour :: Int -> Colour
numToColour 1 = Red
numToColour 2 = Green
numToColour 3 = Yellow
numToColour 4 = Blue
numToColour 5 = Magenta
numToColour _ = Cyan

update :: State -> Char -> State
update state '\x01b' = state { endGame = True }
update state 'r'     = chooseColour state Red
update state 'g'     = chooseColour state Green
update state 'y'     = chooseColour state Yellow
update state 'b'     = chooseColour state Blue
update state 'm'     = chooseColour state Magenta
update state 'c'     = chooseColour state Cyan
update state _       = state

chooseColour :: State -> Colour -> State
chooseColour state colour =
  checkWin $ checkGameOver newState
  where
    newGuess = (currentGuess state) ++ [colour]
    newState = if length newGuess == 4
                  then state { guesses = newGuess : (guesses state), currentGuess = [] }
                  else state { currentGuess = newGuess }

checkWin :: State -> State
checkWin state =
  if evaluateLastGuess (code state) (guesses state)
     then state { endGame = True, won = True }
     else state

checkGameOver :: State -> State
checkGameOver state =
  state { endGame = gameOver, lose = gameOver }
  where
    gameOver = (not $ endGame state) && (not $ won state) && (length (guesses state) == 10)

evaluateLastGuess :: Code -> [Code] -> Bool
evaluateLastGuess _ [] = False
evaluateLastGuess c [guess] = guess == c
evaluateLastGuess c (guess:_guesses) = guess == c

view :: State -> String
view state =
  "\x01b[2J\x01b[0;0H" ++ -- clear screen + go to 0,0
  "MasterMind\n\n" ++
  viewHelp ++ "\n" ++
  (viewGuesses (code state) $ reverse $ guesses state) ++
  "\n    " ++ (viewGuess (currentGuess state)) ++
  viewEndGame state ++
  viewWonLose state

viewGuesses :: Code -> [Code] -> String
viewGuesses target gs =
  intercalate "\n\n" (mapWithInd (viewGuessWithClue target) gs) ++ "\n"

mapWithInd :: (a -> Int -> b) -> [a] -> [b]
mapWithInd f l = zipWith f l [0..]

viewGuess :: Code -> String
viewGuess guess =
  intercalate " " (map viewColour guess)

viewGuessWithClue :: Code -> Code -> Int-> String
viewGuessWithClue target guess index =
  viewGuessNum (index + 1) ++ ") " ++ viewGuess guess ++ " | " ++ viewClue (getClue target guess)

viewGuessNum :: Int -> String
viewGuessNum n = replicate (2 - length strNum) ' ' ++ strNum
  where strNum = show n

viewColour :: Colour -> String
viewColour colour = viewColorText T.Dull (tColor colour) (take 3 $ colourStr colour)

tColor :: Colour -> T.Color
tColor Red     = T.Red
tColor Green   = T.Green
tColor Yellow  = T.Yellow
tColor Blue    = T.Blue
tColor Magenta = T.Magenta
tColor Cyan    = T.Cyan

viewColorText :: T.ColorIntensity -> T.Color -> String -> String
viewColorText cIntensity color text =
  T.setSGRCode [T.SetColor T.Background cIntensity color] ++
  T.setSGRCode [T.SetColor T.Foreground T.Vivid foreground] ++
  text ++ viewStandardColor
  where
    foreground = if color == T.Black then T.Black else T.White

viewStandardColor :: String
viewStandardColor =
  T.setSGRCode [T.SetColor T.Foreground T.Vivid T.White] ++
  T.setSGRCode [T.SetColor T.Background T.Dull T.Black]

viewEndGame :: State -> String
viewEndGame State { endGame = True, code = c } = viewGuess c ++ "\n\n"
viewEndGame _                                  = ""

viewWonLose :: State -> String
viewWonLose State { won = True }    = "    You won!!!\n\n"
viewWonLose State { lose = True }   = "    You lose!!!\n\n"
viewWonLose State { endGame = True} = "\n\n"
viewWonLose _                       = ""

getClue :: Code -> Code -> Clue
getClue target guess =
  Clue { blacks = countRight, whites = countAlmostRight }
  where
    countRight = length $ filter (\x -> (fst x) == (snd x)) zipped
    countAlmostRight = length $ intersectSortedLists (sort $ fst excludingRight) (sort $ snd excludingRight)
    excludingRight = unzip $ filter (\x -> (fst x) /= (snd x)) zipped
    zipped = zip target guess

intersectSortedLists :: Ord a => [a] -> [a] -> [a]
intersectSortedLists (x:xs) (y:ys)
  | x == y  = x : intersectSortedLists xs ys
  | x < y   = intersectSortedLists xs (y:ys)
  | y < x   = intersectSortedLists (x:xs) ys
intersectSortedLists _ _ = []

viewClue :: Clue -> String
viewClue clue =
  intercalate " " $
    replicate (whites clue) (viewClueColour White) ++
    replicate (blacks clue) (viewClueColour Black)

viewClueColour :: ClueColour -> String
viewClueColour colour = viewColorText T.Vivid tc "  "
  where tc = if colour == White then T.White else T.Black

viewHelp :: String
viewHelp =
  "Choose any 4 of the following colours: " ++
  intercalate ", " (map (\c -> viewColorText T.Dull (tColor c) (colourStr c)) [Red, Green, Yellow, Blue, Magenta, Cyan]) ++ "\n" ++
  "Clue: " ++ viewClueColour Black ++ " = right colour in the right space, " ++
  viewClueColour White ++ " = right colour in the wrong space. Clue is not in order.\n" ++
  "Esc = quit game.\n"

colourStr :: Colour -> String
colourStr Red     = "(R)ed"
colourStr Green   = "(G)reen"
colourStr Yellow  = "(Y)ellow"
colourStr Blue    = "(B)lue"
colourStr Magenta = "(M)agenta"
colourStr Cyan    = "(C)yan"
